class WordCounter
  def initialize(file_name)
    @file = File.read(file_name)
  end

  def count
     @file.split.each
  end

  def unique
    @file.split.uniq.length
  end

  def frequency
    count = {}
    @file.split.each do |w|
      if count[w]
        count[w] = count[w] + 1
      else
        count[w] = 1
      end
    end
  end
end


