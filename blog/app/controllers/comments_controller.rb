class CommentsController < ApplicationController
  before_action :set_comment, only: [:create, :destroy]

  def create
    @comment = @post.comments.build(comment_params)

    if @comment.save
      redirect_to @post,
                  notice: 'Comment was successfully created.'
    else
      redirect_to @post,
                  alert: 'Error creating comment.' + @comment.errors.full_messages.to_sentence
    end
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to @post, alert: 'Comment successfully deleted!'
  end

private

  def comment_params
    params.require(:comment).permit(:author, :body, :email, :destroy)
  end

  def set_comment
    @post = Post.find(params[:post_id])
  end
end
